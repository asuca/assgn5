package assgn5;

public class Dancer {
	private int age;
	private String name;
	public void dance() {
		System.out.println("I am a dancer!");
	}
	Dancer(){
	}
	Dancer(int age, String name){
		this.age  = age;
		this.name = name;
	}
	int getAge() {
		return age;
	}
	String getName() {
		return name;
	}
}
