package assgn5;

public class test {
	public static void showInfo(Dancer dancer) {
		System.out.println("Dancer " + dancer.getName() + "'s age is " + dancer.getAge());
		dancer.dance();
	}
	public static void main(String[] args) {
		Dancer[] dancer = new Dancer[]
				{new ElectricBoogieDancer(19,"David"),new Breakdancer(23,"Jim")};
		for (int i = 0; i < dancer.length; i++) {
			showInfo(dancer[i]);
		}
	}
}
