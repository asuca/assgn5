package assgn5;

public class Breakdancer extends Dancer {
	private int age;
	private String name;
	public void dance() {
		System.out.println("He is a breakdancer!");
	}
	Breakdancer(int age,String name){
		this.age  = age;
		this.name = name;
	}
	int getAge() {
		return age;
	}
	String getName() {
		return name;
	}
}
